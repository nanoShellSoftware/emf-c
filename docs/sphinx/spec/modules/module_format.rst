.. _spec_modules_module_format:

#############
Module Format
#############

Modules are a collection of resources.

*****
Goals
*****

- Unified versioning scheme
- Easy dependency management
- Easy composability of multiple modules

*************
Specification
*************

A manifest file shall have the name :code:`module.json`,
and shall be in the root directory of a module.

A manifest file must be a top-level object, and must contain at least:

-  :code:`"schema-version"` : A non negative integer.

Versions
========

Version 0
*********

Supported EMF versions
----------------------
- Version :code:`0.1.0` to :code:`latest`

Required fields
---------------

-  :code:`"name"`: A :code:`string`. Maximum length of 32 ASCII characters.
-  :code:`"module-type"`: A :code:`string`. Maximum length of 64 ASCII characters.
-  :code:`"module-version"`: A :code:`string`. Maximum length of 32 ASCII characters.

Optional fields
---------------

-  :code:`"dependencies"`: An :code:`array` of :code:`<interface-descriptor>`.
-  :code:`"exports"`: An :code:`array` of :code:`<interface-descriptor>`.

Definitions
-----------

- :code:`<interface-descriptor>`: An object which contains:
    - :code:`"name"`: A :code:`string`. Maximum length of 32 ASCII characters.
    - :code:`"version"`: A :code:`string`. See :ref:`spec_common_types_version` for more details.
    - :code:`"extensions"`: An :code:`array` of :code:`string`. Maximum length of 32 ASCII characters per extension (Optional).

Explanations
------------

:code:`"name"`
    The name of a module.

:code:`"module-type"`
    The type of a module.
    Can be queried, to identify an appropriate module loader.

:code:`"module-version"`
    The version of a module.

:code:`"dependencies"`
    A list of interfaces that a module depends on.

:code:`"exports"`
    A list of interfaces that a module exports.

:code:`<interface-descriptor>::"name"`
    The name of an interface.

:code:`<interface-descriptor>::"version"`
    The version of an interface.
    See :ref:`spec_common_types_version` for more details.

:code:`<interface-descriptor>::"extensions"`
    A list of extensions.

Example
-------

.. code-block:: json

    {
        "schema-version": 0,
        "name": "my-module",
        "module-type": "native",
        "module-version": "0.5.0",
        "dependencies": [{
            "name": "a-dependency",
            "version": "1.0.0",
            "extensions": ["dependency-extension"]
        }],
        "exports": [{
            "name": "my-module-export",
            "version": "0.5.0"
        }]
    }

Interface equality
------------------

An interface is considered equal to another interface if their names, versions and extensions are equal.

Interface compatibility
-----------------------

An interface :code:`A` is compatible with the interface :code:`B` if:
    - Their names are equal.
    - :code:`A.version` is compatible to :code:`B.version`.
    - :code:`B.extensions` contains every extension defined in :code:`A.extensions`.

In that case, :code:`A` can be replaced with :code:`B`.

Module equality
---------------

A module is considered equal to another module if both their names and versions are equal.

Module compatibility
--------------------

A module :code:`A` is compatible with the module :code:`B` if:
    - Every interface in :code:`A.exports` is compatible with an interface in :code:`B.exports`

In that case, :code:`A` can be replaced with :code:`B`.
