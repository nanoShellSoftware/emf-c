.. _spec_modules_native_module_format:

####################
Native Module Format
####################

Native modules are modules that can be loaded by the default module loader.

In other words, all modules that:

- Are not linked to any shared library.
- Can be natively loaded by the underlying platform.

All such modules must export a symbol of the type :code:`emf_native_module_interface_t`
and the name :code:`emf_native_module_interface`.

.. note::
    :code:`EMF_NATIVE_MODULE_INTERFACE_SYMBOL_NAME` can be used
    in place of :code:`emf_native_module_interface`.

***************
Module lifetime
***************

The lifetime of a module can be split up in three phases:

- Unloaded.
- Visible.
- Ready.

Unloaded
========

The module has not been added to the system.

Visible
=======

The module is visible to the system and has initialized the minimal amount of resources.

Ready
=====

The module has initialized all the remaining resources, such as interfaces to it's dependencies.
The module is finally usable.

Interface
=========

A native module can enter these phases by calling the following functions:

- :code:`emf_native_module_interface.load_fn` : Unloaded -> Visible.
- :code:`emf_native_module_interface.unload_fn` : Visible -> Unloaded.
- :code:`emf_native_module_interface.initialize_fn` : Visible -> Ready.
- :code:`emf_native_module_interface.terminate_fn` : Ready -> Visible.

:code:`emf_native_module_interface.load_fn`
-------------------------------------------

Initializes any resource, the module makes use of, that do not depend on any external module.

.. note::
    External modules, like the ones the module depends on, may not be loaded at this point.

:code:`emf_native_module_interface.unload_fn`
---------------------------------------------

Terminates any resource that has been acquired with :code:`emf_native_module_interface.load_fn`.

:code:`emf_native_module_interface.initialize_fn`
-------------------------------------------------

Initializes all remaining resources.

.. note::
    It can be assumed, that every dependency has already been initialized.

:code:`emf_native_module_interface.terminate_fn`
------------------------------------------------

Terminates any resource that has been acquired with :code:`emf_native_module_interface.terminate_fn`.

*************
Specification
*************

A manifest file shall have the name :code:`native_module.json`,
and shall be in the root directory of a module.

A manifest file must be a top-level object, and must contain at least:

-  :code:`"schema-version"` : A non negative integer.

Version 0
=========

Supported EMF versions
----------------------
- Version :code:`0.1.0` to :code:`latest`

Required fields
---------------

-  :code:`"library-path"`: A :code:`string`.

Optional fields
---------------

-  :code:`"resources"`: An :code:`array` of :code:`<resource-descriptor>`.

Definitions
-----------

- :code:`<resource-descriptor>`: An object which contains:
    - :code:`"resource-path"`: A :code:`string`.
    - :code:`"mount-name"`: A :code:`string`.
    - :code:`"file-type"`: A :code:`string`.
    - :code:`"write-access"`: A :code:`boolean`.

Explanations
------------

:code:`"library-path"`
    The relative path to the shared library that must be loaded.

:code:`"resources"`
    A list of relative paths that will be mounted into the module directory.

:code:`<resource-descriptor>::"resource-path"`
    A relative path to the resource.

:code:`<resource-descriptor>::"mount-name"`
    A relative path to where the resource will be mounted.

:code:`<resource-descriptor>::"file-type"`
    The file handler that will be used to mount the resource.

:code:`<resource-descriptor>::"write-access"`
    Mount with write permissions.

Example
-------

.. code-block:: json

    {
        "schema-version": 0,
        "library-path": "my_library.so",
        "resources": [{
            "resource-path": "resources/",
            "mount-name": "res",
            "file-type": "emf::core::native",
            "write-access": true
        },
        {
            "resource-path": "libraries/",
            "mount-name": "lib",
            "file-type": "emf::core::native",
            "write_access": false
        }]
    }
