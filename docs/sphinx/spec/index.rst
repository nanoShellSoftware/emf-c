#######
EMF API
#######

*******
Systems
*******

.. toctree::
    :maxdepth: 1
    :name: toc-spec-systems

    systems/config
    systems/core
    systems/event
    systems/file_system
    systems/library
    systems/module

*******
Version
*******

.. toctree::
    :maxdepth: 1
    :name: toc-spec-version

    version/version_utils

*******
Modules
*******

.. toctree::
    :maxdepth: 1
    :name: toc-spec-modules

    modules/module_format
    modules/native_module_format

*************
Miscellaneous
*************

.. toctree::
    :maxdepth: 1
    :name: toc-spec-misc

    misc/type_reference

