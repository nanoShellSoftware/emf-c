.. _spec_systems_config:

######
Config
######

*****
Scope
*****

The Config System provides a centralized means for storing the configuration of the application.

The System can be thought of as a nested collection of Maps.
The *Key* is composed of an optional *Namespace* and a *Property*.

*******
Example
*******

JSON
====

.. code-block:: json

    {
        "str": "test",
        "test_namespace": {
            "double": 0.57,
            "arr": [5, 5],
            "nested_namespace": {
                "bool": true
            }
        }
    }

C Equivalent
============

.. code-block:: c

    emf_config_namespace_t ns1 = { "test_namespace" };
    emf_config_namespace_t ns2 = { "test_namespace::nested_namespace" };

    emf_config_property_t g_str = { "str" };
    emf_config_property_t ns1_double = { "double" };
    emf_config_property_t ns1_arr = { "arr" };
    emf_config_property_t ns2_bool = { "bool" };

    emf_config_string_t g_str_val = { "test", strlen("test") + 1 };

    emf_lock();

    emf_config_add_namespace(&ns1);
    emf_config_add_namespace(&ns2);

    emf_config_add_property_string(NULL, &g_str, 1, 256, &g_str_val);
    emf_config_add_property_real(&ns1, &ns1_double, 1, 0.57);
    emf_config_add_property_integer(&ns1, &ns1_arr, 2, 5);
    emf_config_add_property_boolean(&ns2, &ns2_bool, 1, true);

    emf_unlock();


*********
Functions
*********

System Operations
=================

Management
----------

.. doxygenfunction:: EMF::C::emf_config_add_namespace

.. doxygenfunction:: EMF::C::emf_config_remove_namespace

.. doxygenfunction:: EMF::C::emf_config_add_property_boolean

.. doxygenfunction:: EMF::C::emf_config_add_property_integer

.. doxygenfunction:: EMF::C::emf_config_add_property_real

.. doxygenfunction:: EMF::C::emf_config_add_property_string

.. doxygenfunction:: EMF::C::emf_config_remove_property

.. doxygenfunction:: EMF::C::emf_config_get_num_namespaces

.. doxygenfunction:: EMF::C::emf_config_get_namespaces

.. doxygenfunction:: EMF::C::emf_config_get_num_properties

.. doxygenfunction:: EMF::C::emf_config_get_properties

.. doxygenfunction:: EMF::C::emf_config_namespace_exists

.. doxygenfunction:: EMF::C::emf_config_property_exists

.. doxygenfunction:: EMF::C::emf_config_get_property_type

.. doxygenfunction:: EMF::C::emf_config_get_property_array_size

.. doxygenfunction:: EMF::C::emf_config_get_property_string_size

.. doxygenfunction:: EMF::C::emf_config_get_property_string_max_size

Reading
-------

.. doxygenfunction:: EMF::C::emf_config_property_read_bool

.. doxygenfunction:: EMF::C::emf_config_property_read_integer

.. doxygenfunction:: EMF::C::emf_config_property_read_real

.. doxygenfunction:: EMF::C::emf_config_property_read_string

Writing
-------

.. doxygenfunction:: EMF::C::emf_config_property_write_bool

.. doxygenfunction:: EMF::C::emf_config_property_write_integer

.. doxygenfunction:: EMF::C::emf_config_property_write_real

.. doxygenfunction:: EMF::C::emf_config_property_write_string