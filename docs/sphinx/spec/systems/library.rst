.. _spec_systems_library:

#######
Library
#######

*****
Scope
*****

The Library System is an abstraction over the native shared library loaders.
The management of a library is handled by a *Library Loader*.
A *Library Loader* defines a set of library types, that it can load.

*******
Example
*******

.. code-block:: c

    emf_symbol_t symbol = { };
    emf_path_t library_path = { "/test.so", strlen("/test.so") }

    emf_lock();

    emf_library_handle_t library = emf_library_load(EMF_LIBRARY_LOADER_DEFAULT_HANDLE
        , &library_path);
    
    emf_error_t error = emf_library_get_symbol(library
        , "test_symbol"
        , &symbol);

    emf_unlock();

**************************
Predefined Library Loaders
**************************

.. doxygendefine:: EMF_LIBRARY_LOADER_DEFAULT_HANDLE

The native library loader.

.. note::
    Can load libraries of the type :code:`EMF_NATIVE_LIBRARY_TYPE_NAME`.

*********
Functions
*********

System Operations
=================

Safe Operations
---------------

.. doxygenfunction:: EMF::C::emf_library_register_loader

.. doxygenfunction:: EMF::C::emf_library_unregister_loader

.. doxygenfunction:: EMF::C::emf_library_get_num_loaders

.. doxygenfunction:: EMF::C::emf_library_get_library_types

.. doxygenfunction:: EMF::C::emf_library_get_loader_handle

.. doxygenfunction:: EMF::C::emf_library_type_exists

.. doxygenfunction:: EMF::C::emf_library_library_exists

Unsafe Operations
-----------------

.. doxygenfunction:: EMF::C::emf_library_unsafe_create_library_handle

.. doxygenfunction:: EMF::C::emf_library_unsafe_remove_library_handle

.. doxygenfunction:: EMF::C::emf_library_unsafe_link_library

.. doxygenfunction:: EMF::C::emf_library_unsafe_get_loader_library_handle

.. doxygenfunction:: EMF::C::emf_library_unsafe_get_loader

Loader Operations
=================

.. doxygenfunction:: EMF::C::emf_library_load

.. doxygenfunction:: EMF::C::emf_library_unload

.. doxygenfunction:: EMF::C::emf_library_get_symbol

.. doxygenfunction:: EMF::C::emf_library_get_function_symbol