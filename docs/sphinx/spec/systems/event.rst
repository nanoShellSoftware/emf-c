.. _spec_systems_event:

#####
Event
#####

*****
Scope
*****

The Event System defines an interface for cross-platform and thread-safe signal dispatch.

An *Event* can be available globally, if associated with a name,
and can have zero or more *Handlers* attached to it.

*******
Example
*******

.. code-block:: c

    emf_event_name_t g_event_name = { "example_event" };

    emf_lock();

    emf_event_handle_t g_event = emf_event_create(&g_event_name, NULL);
    emf_event_handle_t p_event = emf_event_create_private(&foo);

    emf_event_subscribe_handler(g_event, &foo);
    emf_event_signal(g_event, NULL);

    emf_unlock();

*********
Functions
*********

System Operations
=================

.. doxygenfunction:: EMF::C::emf_event_create

.. doxygenfunction:: EMF::C::emf_event_create_private

.. doxygenfunction:: EMF::C::emf_event_destroy

.. doxygenfunction:: EMF::C::emf_event_publish

.. doxygenfunction:: EMF::C::emf_event_get_num_public_events

.. doxygenfunction:: EMF::C::emf_event_get_public_events

.. doxygenfunction:: EMF::C::emf_event_get_event_handle

.. doxygenfunction:: EMF::C::emf_event_handle_exists

.. doxygenfunction:: EMF::C::emf_event_name_exists

Event Operations
================

.. doxygenfunction:: EMF::C::emf_event_subscribe_handler

.. doxygenfunction:: EMF::C::emf_event_unsubscribe_handler

.. doxygenfunction:: EMF::C::emf_event_signal