.. _spec_systems_file_system:

###########
File System
###########

*****
Scope
*****

The FS System defines a customizable virtual file system.
This allows for a consistent way of accessing files across platforms.

The virtual fs is only an abstraction over the individual *File Handlers*
and, as such, does not provide any logic on how to unmount/mount a path, read from a file, etc..

*******
Example
*******

.. code-block:: c

    emf_path_t file_path = { "/test", strlen("/test") };
    char buffer[1024];
    emf_fs_buffer_t fs_buffer = { buffer, 1024 };

    emf_lock();

    emf_file_stream_t stream = emf_fs_stream_open(&file_path,
        emf_file_open_mode_begin, emf_file_access_mode_read, NULL, 1024);
    emf_error_t error = emf_fs_stream_read(stream, &fs_buffer, 1024);
    emf_fs_stream_close(stream);

    emf_unlock();

************************
Predefined File Handlers
************************

.. doxygendefine:: EMF_FILE_HANDLER_DEFAULT_HANDLE
    
Interface to the native file system

.. note::
    Can load files of the type :code:`EMF_NATIVE_FILE_HANDLER_FILE_TYPE_NAME`.

.. note::
    No assumptions of its implementation, other than those 
    guaranteed by the interface of the FS System, should be made.

.. warning::
    May not work correctly if used in conjunction with the native C file io.


*********
Functions
*********

System Operations
=================

Safe Operations
---------------

.. doxygenfunction:: EMF::C::emf_fs_register_file_handler

.. doxygenfunction:: EMF::C::emf_fs_remove_file_handler

.. doxygenfunction:: EMF::C::emf_fs_create_file

.. doxygenfunction:: EMF::C::emf_fs_create_link

.. doxygenfunction:: EMF::C::emf_fs_create_directory

.. doxygenfunction:: EMF::C::emf_fs_delete

.. doxygenfunction:: EMF::C::emf_fs_mount_memory_file

.. doxygenfunction:: EMF::C::emf_fs_mount_native_path

.. doxygenfunction:: EMF::C::emf_fs_unmount

.. doxygenfunction:: EMF::C::emf_fs_set_access_mode

.. doxygenfunction:: EMF::C::emf_fs_get_access_mode

.. doxygenfunction:: EMF::C::emf_fs_get_mount_id

.. doxygenfunction:: EMF::C::emf_fs_can_access

.. doxygenfunction:: EMF::C::emf_fs_can_set_access_mode

.. doxygenfunction:: EMF::C::emf_fs_is_virtual

.. doxygenfunction:: EMF::C::emf_fs_can_delete

.. doxygenfunction:: EMF::C::emf_fs_can_mount_type

.. doxygenfunction:: EMF::C::emf_fs_can_mount_native_path

.. doxygenfunction:: EMF::C::emf_fs_get_num_entries

.. doxygenfunction:: EMF::C::emf_fs_get_entries

.. doxygenfunction:: EMF::C::emf_fs_exists

.. doxygenfunction:: EMF::C::emf_fs_type_exists

.. doxygenfunction:: EMF::C::emf_fs_get_entry_type

.. doxygenfunction:: EMF::C::emf_fs_resolve_link

.. doxygenfunction:: EMF::C::emf_fs_get_link_type

.. doxygenfunction:: EMF::C::emf_fs_get_size

.. doxygenfunction:: EMF::C::emf_fs_get_native_path_length

.. doxygenfunction:: EMF::C::emf_fs_get_native_path

.. doxygenfunction:: EMF::C::emf_fs_get_num_file_handlers

.. doxygenfunction:: EMF::C::emf_fs_get_file_handlers

.. doxygenfunction:: EMF::C::emf_fs_get_file_handler_from_type

.. doxygenfunction:: EMF::C::emf_fs_get_num_file_types

.. doxygenfunction:: EMF::C::emf_fs_get_file_types

.. doxygenfunction:: EMF::C::emf_fs_get_num_handler_file_types

.. doxygenfunction:: EMF::C::emf_fs_get_handler_file_types

.. doxygenfunction:: EMF::C::emf_fs_normalize

.. doxygenfunction:: EMF::C::emf_fs_get_parent

.. doxygenfunction:: EMF::C::emf_fs_join

Unsafe Operations
-----------------

.. doxygenfunction:: EMF::C::emf_fs_unsafe_create_mount_id

.. doxygenfunction:: EMF::C::emf_fs_unsafe_remove_mount_id

.. doxygenfunction:: EMF::C::emf_fs_unsafe_unmount_force

.. doxygenfunction:: EMF::C::emf_fs_unsafe_link_mount_point

.. doxygenfunction:: EMF::C::emf_fs_unsafe_create_file_stream

.. doxygenfunction:: EMF::C::emf_fs_unsafe_remove_file_stream

.. doxygenfunction:: EMF::C::emf_fs_unsafe_link_file_stream

.. doxygenfunction:: EMF::C::emf_fs_unsafe_get_file_handler_handle_from_stream

.. doxygenfunction:: EMF::C::emf_fs_unsafe_get_file_handler_handle_from_path

.. doxygenfunction:: EMF::C::emf_fs_unsafe_get_file_handler_stream

.. doxygenfunction:: EMF::C::emf_fs_unsafe_get_file_handler_mount_id

.. doxygenfunction:: EMF::C::emf_fs_unsafe_get_file_handler

File Stream Operations
======================

.. doxygenfunction:: EMF::C::emf_fs_stream_open

.. doxygenfunction:: EMF::C::emf_fs_stream_close

.. doxygenfunction:: EMF::C::emf_fs_stream_flush

.. doxygenfunction:: EMF::C::emf_fs_stream_read

.. doxygenfunction:: EMF::C::emf_fs_stream_write

.. doxygenfunction:: EMF::C::emf_fs_stream_get_pos

.. doxygenfunction:: EMF::C::emf_fs_stream_set_pos

.. doxygenfunction:: EMF::C::emf_fs_stream_move_pos

.. doxygenfunction:: EMF::C::emf_fs_stream_can_write

.. doxygenfunction:: EMF::C::emf_fs_stream_can_grow