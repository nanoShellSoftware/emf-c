.. _spec_systems_module:

######
Module
######

*****
Scope
*****

The Module System is an abstraction over the Library System.
It's purpose is to load and manage modules, and their dependencies.

*******
Example
*******

.. code-block:: c

    emf_path_t module_path = { "/test_module/", strlen("/test_module/") }

    emf_lock();
    emf_module_handle_t module = emf_module_load(EMF_MODULE_LOADER_DEFAULT_HANDLE
        , NULL
        , &module_path);

    emf_module_initialize(system_lock, module);
    emf_unlock();

*************************
Predefined Module Loaders
*************************

.. doxygendefine:: EMF_MODULE_LOADER_DEFAULT_HANDLE

A loader that can load native modules.

.. note::
    Can load libraries of the type :code:`EMF_NATIVE_MODULE_TYPE_NAME`.

*********
Functions
*********

System Operations
=================

Safe Operations
---------------

.. doxygenfunction:: EMF::C::emf_module_register_loader

.. doxygenfunction:: EMF::C::emf_module_unregister_loader

.. doxygenfunction:: EMF::C::emf_module_get_num_loaders

.. doxygenfunction:: EMF::C::emf_module_get_module_types

.. doxygenfunction:: EMF::C::emf_module_get_num_modules

.. doxygenfunction:: EMF::C::emf_module_get_modules

.. doxygenfunction:: EMF::C::emf_module_get_num_public_interfaces

.. doxygenfunction:: EMF::C::emf_module_get_public_interfaces

.. doxygenfunction:: EMF::C::emf_module_get_loader_handle

.. doxygenfunction:: EMF::C::emf_module_type_exists

.. doxygenfunction:: EMF::C::emf_module_module_exists

.. doxygenfunction:: EMF::C::emf_module_get_interface_handle

.. doxygenfunction:: EMF::C::emf_module_interface_exists

Unsafe Operations
-----------------

.. doxygenfunction:: EMF::C::emf_module_unsafe_create_module_handle

.. doxygenfunction:: EMF::C::emf_module_unsafe_remove_module_handle

.. doxygenfunction:: EMF::C::emf_module_unsafe_link_module

.. doxygenfunction:: EMF::C::emf_module_unsafe_get_loader_module_handle

.. doxygenfunction:: EMF::C::emf_module_unsafe_get_loader

Loader Operations
=================

.. doxygenfunction:: EMF::C::emf_module_load

.. doxygenfunction:: EMF::C::emf_module_unload

.. doxygenfunction:: EMF::C::emf_module_initialize

.. doxygenfunction:: EMF::C::emf_module_terminate

.. doxygenfunction:: EMF::C::emf_module_get_module_info

.. doxygenfunction:: EMF::C::emf_module_get_exported_interfaces

.. doxygenfunction:: EMF::C::emf_module_get_imported_interfaces

.. doxygenfunction:: EMF::C::emf_module_get_interface