.. _spec_systems_core:

####
Core
####

*****
Scope
*****

The Core System provides some basic functionality for interfacing with the application.

*********
Functions
*********

.. doxygenfunction:: EMF::C::emf_lock

.. doxygenfunction:: EMF::C::emf_try_lock

.. doxygenfunction:: EMF::C::emf_unlock

.. doxygenfunction:: EMF::C::emf_shutdown

.. doxygenfunction:: EMF::C::emf_panic

.. doxygenfunction:: EMF::C::emf_has_function

.. doxygenfunction:: EMF::C::emf_get_function

.. doxygenfunction:: EMF::C::emf_get_sync_handler

.. doxygenfunction:: EMF::C::emf_set_sync_handler