.. _spec_version_version_utils:

#############
Version utils
#############

*****
Scope
*****

Some utils to create, serialize and compare versions.

*********
Functions
*********

.. doxygenfunction:: EMF::C::emf_version_construct_short

.. doxygenfunction:: EMF::C::emf_version_construct_long

.. doxygenfunction:: EMF::C::emf_version_construct_full

.. doxygenfunction:: EMF::C::emf_version_construct_from_string

.. doxygenfunction:: EMF::C::emf_version_representation_is_valid

.. doxygenfunction:: EMF::C::emf_version_get_short_representation

.. doxygenfunction:: EMF::C::emf_version_get_short_representation_size

.. doxygenfunction:: EMF::C::emf_version_get_long_representation

.. doxygenfunction:: EMF::C::emf_version_get_long_representation_size

.. doxygenfunction:: EMF::C::emf_version_get_full_representation

.. doxygenfunction:: EMF::C::emf_version_get_full_representation_size

.. doxygenfunction:: EMF::C::emf_version_compare

.. doxygenfunction:: EMF::C::emf_version_compare_weak

.. doxygenfunction:: EMF::C::emf_version_compare_strong

.. doxygenfunction:: EMF::C::emf_version_is_compatible