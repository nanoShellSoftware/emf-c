.. _spec_misc_fs_emf_pos_t:

#################
:code:`emf_pos_t`
#################

*******
Summary
*******

The position in a stream.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_pos_t
    :members:
    :undoc-members:
