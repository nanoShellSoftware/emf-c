.. _spec_misc_fs_emf_fs_link_t:

#####################
:code:`emf_fs_link_t`
#####################

*******
Summary
*******

The types of filesystem links.

**********
Definition
**********

.. doxygenenum:: EMF::C::emf_fs_link_t
