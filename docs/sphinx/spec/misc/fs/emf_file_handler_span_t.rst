.. _spec_misc_fs_emf_file_handler_span_t:

###############################
:code:`emf_file_handler_span_t`
###############################

*******
Summary
*******

A span of filesystem file handlers.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_file_handler_span_t
    :members:
    :undoc-members:
