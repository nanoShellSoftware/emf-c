.. _spec_misc_fs_emf_file_handler_mount_id_t:

###################################
:code:`emf_file_handler_mount_id_t`
###################################

*******
Summary
*******

The internal handle to amount point.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_file_handler_mount_id_t
    :members:
    :undoc-members:
