.. _spec_misc_fs_emf_memory_span_t:

#########################
:code:`emf_memory_span_t`
#########################

*******
Summary
*******

A span over a continuous region of memory.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_memory_span_t
    :members:
    :undoc-members:
