.. _spec_misc_fs_emf_mount_id_t:

######################
:code:`emf_mount_id_t`
######################

*******
Summary
*******

The handle to a mount point.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_mount_id_t
    :members:
    :undoc-members:
