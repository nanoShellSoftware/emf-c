.. _spec_misc_fs_emf_file_handler_t:

###########################
 :code:`emf_file_handler_t`
###########################

*******
Summary
*******

The handle to a file handler.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_file_handler_t
    :members:
    :undoc-members:
