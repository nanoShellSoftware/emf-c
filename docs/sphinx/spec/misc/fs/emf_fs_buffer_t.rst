.. _spec_misc_fs_emf_fs_buffer_t:

#######################
:code:`emf_fs_buffer_t`
#######################

*******
Summary
*******

A buffer for the filesystem system.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_fs_buffer_t
    :members:
    :undoc-members:
