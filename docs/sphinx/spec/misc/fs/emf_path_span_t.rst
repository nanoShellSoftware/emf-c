.. _spec_misc_fs_emf_path_span_t:

#######################
:code:`emf_path_span_t`
#######################

*******
Summary
*******

A span of paths.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_path_span_t
    :members:
    :undoc-members:
