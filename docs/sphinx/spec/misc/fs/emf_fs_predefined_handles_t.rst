.. _spec_misc_fs_emf_fs_predefined_handles_t:

###################################
:code:`emf_fs_predefined_handles_t`
###################################

*******
Summary
*******

The handles to the predefined filesystem file handlers.

**********
Definition
**********

.. doxygenenum:: EMF::C::emf_fs_predefined_handles_t
