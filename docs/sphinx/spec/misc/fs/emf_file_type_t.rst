.. _spec_misc_fs_emf_file_type_t:

#######################
:code:`emf_file_type_t`
#######################

*******
Summary
*******

The type of a file.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_file_type_t
    :members:
    :undoc-members:
