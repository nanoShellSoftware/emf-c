.. _spec_misc_fs_emf_off_t:

#################
:code:`emf_off_t`
#################

*******
Summary
*******

The difference of two :cpp:struct:`EMF::C::emf_pos_t`

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_off_t
    :members:
    :undoc-members:
