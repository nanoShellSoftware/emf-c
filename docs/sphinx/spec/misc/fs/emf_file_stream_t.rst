.. _spec_misc_fs_emf_file_stream_t:

#########################
:code:`emf_file_stream_t`
#########################

*******
Summary
*******

The handle to an opened file.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_file_stream_t
    :members:
    :undoc-members:
