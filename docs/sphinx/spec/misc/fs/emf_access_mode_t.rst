.. _spec_misc_fs_emf_access_mode_t:

#########################
:code:`emf_access_mode_t`
#########################

*******
Summary
*******

The filesystem read/write permissions.

**********
Definition
**********

.. doxygenenum:: EMF::C::emf_access_mode_t
