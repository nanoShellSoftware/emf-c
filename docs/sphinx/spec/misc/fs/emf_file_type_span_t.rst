.. _spec_misc_fs_emf_file_type_span_t:

############################
:code:`emf_file_type_span_t`
############################

*******
Summary
*******

The span of file types.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_file_type_span_t
    :members:
    :undoc-members:
