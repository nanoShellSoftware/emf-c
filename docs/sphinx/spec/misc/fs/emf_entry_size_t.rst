.. _spec_misc_fs_emf_entry_size_t:

########################
:code:`emf_entry_size_t`
########################

*******
Summary
*******

The size of a filesystem entry.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_entry_size_t
    :members:
    :undoc-members:
