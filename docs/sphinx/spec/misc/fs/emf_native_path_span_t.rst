.. _spec_misc_fs_emf_native_path_span_t:

##############################
:code:`emf_native_path_span_t`
##############################

*******
Summary
*******

A buffer of characters wide enough to store a path.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_native_path_span_t
    :members:
    :undoc-members:
