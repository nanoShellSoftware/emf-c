.. _spec_misc_fs_emf_file_open_mode_t:

############################
:code:`emf_file_open_mode_t`
############################

*******
Summary
*******

The mode in witch to open a file.

**********
Definition
**********

.. doxygenenum:: EMF::C::emf_file_open_mode_t
