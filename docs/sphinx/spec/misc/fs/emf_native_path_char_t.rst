.. _spec_misc_fs_emf_native_path_char_t:

##############################
:code:`emf_native_path_char_t`
##############################

*******
Summary
*******

The character type of the platforms filesystem.

**********
Definition
**********

.. doxygentypedef:: EMF::C::emf_native_path_char_t
