.. _spec_misc_fs_emf_file_handler_stream_t:

#################################
:code:`emf_file_handler_stream_t`
#################################

*******
Summary
*******

The internal handle to an opened file.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_file_handler_stream_t
    :members:
    :undoc-members:
