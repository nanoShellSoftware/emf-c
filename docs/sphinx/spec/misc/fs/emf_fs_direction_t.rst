.. _spec_misc_fs_emf_fs_direction_t:

##########################
:code:`emf_fs_direction_t`
##########################

*******
Summary
*******

The direction of a filesystem seek operation.

**********
Definition
**********

.. doxygenenum:: EMF::C::emf_fs_direction_t
