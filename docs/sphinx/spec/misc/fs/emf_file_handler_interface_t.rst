.. _spec_misc_fs_emf_file_handler_interface_t:

####################################
:code:`emf_file_handler_interface_t`
####################################

*******
Summary
*******

The interface of a filesystem file handler.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_file_handler_interface_t
    :members:
    :undoc-members:
