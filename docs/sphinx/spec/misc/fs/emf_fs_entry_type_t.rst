.. _spec_misc_fs_emf_fs_entry_type_t:

###########################
:code:`emf_fs_entry_type_t`
###########################

*******
Summary
*******

The type of a filesystem entry.

**********
Definition
**********

.. doxygenenum:: EMF::C::emf_fs_entry_type_t
