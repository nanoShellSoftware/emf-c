.. _spec_misc_fs_emf_path_t:

##################
:code:`emf_path_t`
##################

*******
Summary
*******

A path.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_path_t
    :members:
    :undoc-members:
