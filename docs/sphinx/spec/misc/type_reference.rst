.. _spec_misc_type_reference:

##############
Type reference
##############

******
Common
******

.. toctree::
    :maxdepth: 1
    :name: toc-spec-misc-types-common

    common/emf_bool_t
    common/emf_error_t
    common/emf_fn_ptr_id_t
    common/emf_version_release_t
    common/emf_version_representation_buffer_t
    common/emf_version_t

****
Core
****

.. toctree::
    :maxdepth: 1
    :name: toc-spec-misc-types-core

    core/emf_sync_handler_t

******
Config
******

.. toctree::
    :maxdepth: 1
    :name: toc-spec-misc-types-config

    config/emf_config_namespace_span_t
    config/emf_config_namespace_t
    config/emf_config_property_info_span_t
    config/emf_config_property_info_t
    config/emf_config_property_t
    config/emf_config_property_type_t
    config/emf_config_string_t

*****
Event
*****

.. toctree::
    :maxdepth: 1
    :name: toc-spec-misc-types-event

    event/emf_event_handle_t
    event/emf_event_name_span_t
    event/emf_event_name_t

**********
Filesystem
**********

.. toctree::
    :maxdepth: 1
    :name: toc-spec-misc-types-fs

    fs/emf_access_mode_t
    fs/emf_entry_size_t
    fs/emf_file_handler_interface_t
    fs/emf_file_handler_mount_id_t
    fs/emf_file_handler_span_t
    fs/emf_file_handler_stream_t
    fs/emf_file_handler_t
    fs/emf_file_open_mode_t
    fs/emf_file_stream_t
    fs/emf_file_type_span_t
    fs/emf_file_type_t
    fs/emf_fs_buffer_t
    fs/emf_fs_direction_t
    fs/emf_fs_entry_type_t
    fs/emf_fs_link_t
    fs/emf_fs_predefined_handles_t
    fs/emf_memory_span_t
    fs/emf_mount_id_t
    fs/emf_native_path_char_t
    fs/emf_native_path_span_t
    fs/emf_off_t
    fs/emf_path_span_t
    fs/emf_path_t
    fs/emf_pos_t

*******
Library
*******

.. toctree::
    :maxdepth: 1
    :name: toc-spec-misc-types-library

    library/emf_fn_symbol_t
    library/emf_library_handle_t
    library/emf_library_loader_handle_t
    library/emf_library_loader_interface_t
    library/emf_library_loader_library_handle_t
    library/emf_library_predefined_handles_t
    library/emf_library_type_span_t
    library/emf_library_type_t
    library/emf_symbol_t

******
Module
******

.. toctree::
    :maxdepth: 1
    :name: toc-spec-misc-types-module

    module/emf_interface_descriptor_span_t
    module/emf_interface_descriptor_t
    module/emf_interface_extension_span_t
    module/emf_interface_extension_t
    module/emf_interface_info_span_t
    module/emf_interface_info_t
    module/emf_interface_name_t
    module/emf_interface_t
    module/emf_module_handle_t
    module/emf_module_info_span_t
    module/emf_module_info_t
    module/emf_module_loader_handle_t
    module/emf_module_loader_interface_t
    module/emf_module_loader_module_handle_t
    module/emf_module_name_t
    module/emf_module_predefined_handles_t
    module/emf_module_type_span_t
    module/emf_module_type_t
    module/emf_module_version_t
    module/emf_native_module_interface_t
