.. _spec_misc_core_emf_sync_handler_t:

##########################
:code:`emf_sync_handler_t`
##########################

*******
Summary
*******

The interface of a synchronisation handler.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_sync_handler_t
    :members:
    :undoc-members:
