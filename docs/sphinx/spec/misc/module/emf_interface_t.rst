.. _spec_misc_module_emf_interface_t:

#######################
:code:`emf_interface_t`
#######################

*******
Summary
*******

A pointer to an interface.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_interface_t
    :members:
    :undoc-members:
