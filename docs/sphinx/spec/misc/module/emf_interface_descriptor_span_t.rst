.. _spec_misc_module_emf_interface_descriptor_span_t:

#######################################
:code:`emf_interface_descriptor_span_t`
#######################################

*******
Summary
*******

A structure that describes the name, version and extensions of an interface.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_interface_descriptor_span_t
    :members:
    :undoc-members:
