.. _spec_misc_module_emf_module_version_t:

############################
:code:`emf_module_version_t`
############################

*******
Summary
*******

A structure that describes the version of a module.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_module_version_t
    :members:
    :undoc-members:
