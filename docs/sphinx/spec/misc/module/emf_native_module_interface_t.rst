.. _spec_misc_module_emf_native_module_interface_t:

#####################################
:code:`emf_native_module_interface_t`
#####################################

*******
Summary
*******

The interface of native modules.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_native_module_interface_t
    :members:
    :undoc-members:
