.. _spec_misc_module_emf_module_loader_module_handle_t:

#########################################
:code:`emf_module_loader_module_handle_t`
#########################################

*******
Summary
*******

The internal module handle used by the module loader.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_module_loader_module_handle_t
    :members:
    :undoc-members:
