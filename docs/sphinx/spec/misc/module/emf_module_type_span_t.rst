.. _spec_misc_module_emf_module_type_span_t:

##############################
:code:`emf_module_type_span_t`
##############################

*******
Summary
*******

A span of module types.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_module_type_span_t
    :members:
    :undoc-members:
