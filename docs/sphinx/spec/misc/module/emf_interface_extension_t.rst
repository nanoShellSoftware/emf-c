.. _spec_misc_module_emf_interface_extension_t:

#################################
:code:`emf_interface_extension_t`
#################################

*******
Summary
*******

The name of an interface extension.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_interface_extension_t
    :members:
    :undoc-members:
