.. _spec_misc_module_emf_interface_info_span_t:

#################################
:code:`emf_interface_info_span_t`
#################################

*******
Summary
*******

A span of interface infos.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_interface_info_span_t
    :members:
    :undoc-members:
