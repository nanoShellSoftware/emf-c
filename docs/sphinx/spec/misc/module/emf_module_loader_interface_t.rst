.. _spec_misc_module_emf_module_loader_interface_t:

#####################################
:code:`emf_module_loader_interface_t`
#####################################

*******
Summary
*******

The interface of a module loader.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_module_loader_interface_t
    :members:
    :undoc-members:
