.. _spec_misc_module_emf_module_name_t:

#########################
:code:`emf_module_name_t`
#########################

*******
Summary
*******

A structure that describes the name of a module.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_module_name_t
    :members:
    :undoc-members:
