.. _spec_misc_module_emf_module_type_t:

#########################
:code:`emf_module_type_t`
#########################

*******
Summary
*******

Textual description of the module type.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_module_type_t
    :members:
    :undoc-members:
