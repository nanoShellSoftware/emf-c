.. _spec_misc_module_emf_module_predefined_handles_t:

#######################################
:code:`emf_module_predefined_handles_t`
#######################################

*******
Summary
*******

The handles to the predefined module loaders.

**********
Definition
**********

.. doxygenenum:: EMF::C::emf_module_predefined_handles_t
