.. _spec_misc_module_emf_interface_extension_span_t:

######################################
:code:`emf_interface_extension_span_t`
######################################

*******
Summary
*******

A span of interface extensions.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_interface_extension_span_t
    :members:
    :undoc-members:
