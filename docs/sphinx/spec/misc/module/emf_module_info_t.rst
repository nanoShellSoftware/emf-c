.. _spec_misc_module_emf_module_info_t:

#########################
:code:`emf_module_info_t`
#########################

*******
Summary
*******

A structure that describes a module.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_module_info_t
    :members:
    :undoc-members:
