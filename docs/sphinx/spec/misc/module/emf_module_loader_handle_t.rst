.. _spec_misc_module_emf_module_loader_handle_t:

##################################
:code:`emf_module_loader_handle_t`
##################################

*******
Summary
*******

A handle to a module loader.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_module_loader_handle_t
    :members:
    :undoc-members:
