.. _spec_misc_module_emf_interface_info_t:

############################
:code:`emf_interface_info_t`
############################

*******
Summary
*******

A structure that describes the name and version of an interface.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_interface_info_t
    :members:
    :undoc-members:
