.. _spec_misc_module_emf_interface_name_t:

############################
:code:`emf_interface_name_t`
############################

*******
Summary
*******

A structure that describes the name of an interface.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_interface_name_t
    :members:
    :undoc-members:
