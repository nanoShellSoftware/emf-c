.. _spec_misc_common_emf_version_release_t:

#############################
:code:`emf_version_release_t`
#############################

*******
Summary
*******

The release type of a version.

**********
Definition
**********

.. doxygenenum:: EMF::C::emf_version_release_t
