.. _spec_misc_common_emf_bool_t:

##################
:code:`emf_bool_t`
##################

*******
Summary
*******

A boolean value.

**********
Definition
**********

.. doxygenenum:: EMF::C::emf_bool_t
