.. _spec_misc_common_emf_version_t:

#####################
:code:`emf_version_t`
#####################

*******
Summary
*******

A version.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_version_t
    :members:
    :undoc-members:
