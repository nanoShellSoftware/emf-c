.. _spec_misc_common_emf_version_representation_buffer_t:

###########################################
:code:`emf_version_representation_buffer_t`
###########################################

*******
Summary
*******

A buffer that contains the string representation of a version.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_version_representation_buffer_t
    :members:
    :undoc-members:
