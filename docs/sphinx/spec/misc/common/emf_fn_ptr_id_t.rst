.. _spec_misc_common_emf_fn_ptr_id_t:

#######################
:code:`emf_fn_ptr_id_t`
#######################

*******
Summary
*******

The id's of every function.

**********
Definition
**********

.. doxygenenum:: EMF::C::emf_fn_ptr_id_t
