.. _spec_misc_common_emf_error_t:

###################
:code:`emf_error_t`
###################

*******
Summary
*******

An error value.

**********
Definition
**********

.. doxygenenum:: EMF::C::emf_error_t
