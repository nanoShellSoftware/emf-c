.. _spec_misc_library_emf_library_loader_interface_t:

######################################
:code:`emf_library_loader_interface_t`
######################################

*******
Summary
*******

The interface of a library loader.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_library_loader_interface_t
    :members:
    :undoc-members:
