.. _spec_misc_library_emf_library_type_span_t:

###############################
:code:`emf_library_type_span_t`
###############################

*******
Summary
*******

A span of library types.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_library_type_span_t
    :members:
    :undoc-members:
