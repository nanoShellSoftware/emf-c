.. _spec_misc_library_emf_fn_symbol_t:

#######################
:code:`emf_fn_symbol_t`
#######################

*******
Summary
*******

A pointer to an exported function symbol of a library.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_fn_symbol_t
    :members:
    :undoc-members:
