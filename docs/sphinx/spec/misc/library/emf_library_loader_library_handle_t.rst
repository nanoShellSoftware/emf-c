.. _spec_misc_library_emf_library_loader_library_handle_t:

###########################################
:code:`emf_library_loader_library_handle_t`
###########################################

*******
Summary
*******

A library handle used by a library loader.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_library_loader_library_handle_t
    :members:
    :undoc-members:
