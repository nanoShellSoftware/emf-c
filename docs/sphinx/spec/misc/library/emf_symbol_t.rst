.. _spec_misc_library_emf_symbol_t:

####################
:code:`emf_symbol_t`
####################

*******
Summary
*******

A pointer to an exported symbol of a library.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_symbol_t
    :members:
    :undoc-members:
