.. _spec_misc_library_emf_library_type_t:

##########################
:code:`emf_library_type_t`
##########################

*******
Summary
*******

Textual description of the library type.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_library_type_t
    :members:
    :undoc-members:
