.. _spec_misc_library_emf_library_loader_handle_t:

###################################
:code:`emf_library_loader_handle_t`
###################################

*******
Summary
*******

A handle to a library loader.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_library_loader_handle_t
    :members:
    :undoc-members:
