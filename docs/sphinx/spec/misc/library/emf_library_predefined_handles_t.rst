.. _spec_misc_library_emf_library_predefined_handles_t:

########################################
:code:`emf_library_predefined_handles_t`
########################################

*******
Summary
*******

The handles to the predefined library loaders.

**********
Definition
**********

.. doxygenenum:: EMF::C::emf_library_predefined_handles_t
