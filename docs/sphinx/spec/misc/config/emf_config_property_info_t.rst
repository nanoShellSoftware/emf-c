.. _spec_misc_config_emf_config_property_info_t:

##################################
:code:`emf_config_property_info_t`
##################################

*******
Summary
*******

Information of a property.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_config_property_info_t
    :members:
    :undoc-members:
