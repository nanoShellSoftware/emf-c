.. _spec_misc_config_emf_config_property_type_t:

##################################
:code:`emf_config_property_type_t`
##################################

*******
Summary
*******

The type of a property.

**********
Definition
**********

.. doxygenenum:: EMF::C::emf_config_property_type_t
