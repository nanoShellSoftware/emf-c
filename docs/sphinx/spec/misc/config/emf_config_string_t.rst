.. _spec_misc_config_emf_config_string_t:

###########################
:code:`emf_config_string_t`
###########################

*******
Summary
*******

The container of a string property.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_config_string_t
    :members:
    :undoc-members:
