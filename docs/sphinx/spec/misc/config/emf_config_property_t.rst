.. _spec_misc_config_emf_config_property_t:

#############################
:code:`emf_config_property_t`
#############################

*******
Summary
*******

The identifier of a property.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_config_property_t
    :members:
    :undoc-members:
