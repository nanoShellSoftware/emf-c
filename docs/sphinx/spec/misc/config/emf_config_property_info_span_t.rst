.. _spec_misc_config_emf_config_property_info_span_t:

#######################################
:code:`emf_config_property_info_span_t`
#######################################

*******
Summary
*******

A span of property infos.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_config_property_info_span_t
    :members:
    :undoc-members:
