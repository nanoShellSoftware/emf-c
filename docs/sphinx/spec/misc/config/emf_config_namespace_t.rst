.. _spec_misc_config_emf_config_namespace_t:

##############################
:code:`emf_config_namespace_t`
##############################

*******
Summary
*******

The identifier of a namespace.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_config_namespace_t
    :members:
    :undoc-members:
