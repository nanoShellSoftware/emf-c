.. _spec_misc_config_emf_config_namespace_span_t:

###################################
:code:`emf_config_namespace_span_t`
###################################

*******
Summary
*******

A span of namespaces.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_config_namespace_span_t
    :members:
    :undoc-members:
