.. _spec_misc_event_emf_event_handle_t:

##########################
:code:`emf_event_handle_t`
##########################

*******
Summary
*******

The handle to an event.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_event_handle_t
    :members:
    :undoc-members:
