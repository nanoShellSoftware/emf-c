.. _spec_misc_event_emf_event_name_t:

########################
:code:`emf_event_name_t`
########################

*******
Summary
*******

The name of an event.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_event_name_t
    :members:
    :undoc-members:
