.. _spec_misc_event_emf_event_name_span_t:

#############################
:code:`emf_event_name_span_t`
#############################

*******
Summary
*******

A span of event names.

**********
Definition
**********

.. doxygenstruct:: EMF::C::emf_event_name_span_t
    :members:
    :undoc-members:
