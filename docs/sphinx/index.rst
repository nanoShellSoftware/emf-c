###########################################
Welcome to EMF-C's documentation!
###########################################

The documentation is organized into the following sections:

*******
General
*******

.. toctree::
   :maxdepth: 1
   :name: sec-general

   general/index

*************
Specification
*************

.. toctree::
   :maxdepth: 1
   :name: sec-spec

   spec/index