.. _general_introduction:

############
Introduction
############

Welcome to the documentation of EMF-C.

***********
About EMF-C
***********

EMF-C stands for Engine-Module-Framework-C interface and is, 
as the name suggests, a specification of a framework for creating
a modular game engine.

It consists of a collection of systems.
These systems are:

* Config system
* Core system
* Event system
* FS system
* Library system
* Module system

************
Contributing
************

The project can be found on `GitLab <https://gitlab.com/nanoShellSoftware/emf-c>`_.

You can help out by improving the documentation,
writing some tests or submitting new ideas.

.. note::
    EMF-C is licensed under the MIT license.