# Welcome to the Engine-Module-Framework C interface repo

This repo contains the following:
- The EMF C interface
- Documentation of the interface
- A set of tests for the implementers of the EMF interface

## Prerequisites

- Git
- CMake (version >= 3.15)
- C++20
- Python 3
- Doxygen
- Sphinx
    - Breathe
    - sphinx_rtd_theme

## Documentation

The documentation can be found in the `./docs` folder and can be generated with CMake.

## Building

You can build the project using cmake.
<br>
E.g.:
```
git submodule update --init --recursive
./vcpkg/bootstrap-vcpkg
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release -G Ninja -DVCPKG_TARGET_TRIPLET=x64-windows -DCMAKE_TOOLCHAIN_FILE=./vcpkg/scripts/buildsystems/vcpkg.cmake
cmake -build
```